<?php

use App\Http\Controllers\Web\ProjectController;
use App\Http\Controllers\Web\TaskController;
use App\Http\Controllers\Web\WebsiteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Home */
Route::get('/', [WebsiteController::class, 'index']);

/* Projects */
Route::resource('projects', ProjectController::class, ['only' => [
    'index', 'create', 'show', 'edit'
]]);

/* Tasks */
Route::resource('tasks', TaskController::class, ['only' => [
    'create', 'edit'
]]);
