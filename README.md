### Getting started:
* Copy `.env` file in the project root folder so run following command  `cp .env.example .env`
* In `.env` file update the database info
* Open the terminal or command prompt and navigate to the project directory and run `composer update`
* Run migration `php artisan migrate`
* Generate the application key using `php artisan key:generate`
* Clear the config cache by running `php artisan config:cache`
* NOTE: incase if you are getting any error Target class does not exist then please run `composer dump-autoload`
